#language: es
#Autor: mateoserna.35@gmail.com

  Característica: Yo como analista de canales necesito visualizar el listado de
    usuarios que tengo en mi sistema

    @ValidarRespuesta
    Escenario: El usuario valida el codigo de respuesta exitoso
      Cuando El usuario hace la peticion al servicio
      Entonces El usuario debera ver el codigo de estado 200
      Y El usuario obtendra una respuesta exitosa