#language: es
#Autor: mateoserna.35@gmail.com

  Característica: Yo como analista de canales necesito crear los usuarios solicitados
    para tener registro de estos

  @CrearServicioExitoso
  Esquema del escenario: El usuario valida la creacion de la peticion al servicio
    Cuando El usuario ingresa la siguiente informacion
      | name   | job   |
      | <name> | <job> |
    Entonces El usuario obtendra un codigo de respuesta 201
    Y El usuario podra ver la siguiente informacion
      | name   | job   | id   | createdAt   |
      | <name> | <job> | <id> | <createdAt> |
    Ejemplos:
      | name | job                 |
      | Jose | Analista de calidad |
