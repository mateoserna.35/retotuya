package co.com.tuya.certification.servicio.runners;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/obtener_servicio.feature",
        glue = "co.com.tuya.certification.servicio.stepdefinitions",
        snippets = SnippetType.CAMELCASE
)

public class ObtenerServicio {
}