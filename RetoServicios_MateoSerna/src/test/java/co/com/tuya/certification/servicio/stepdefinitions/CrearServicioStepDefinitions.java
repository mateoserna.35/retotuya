package co.com.tuya.certification.servicio.stepdefinitions;

import co.com.tuya.certification.servicio.models.DatosCrear;
import co.com.tuya.certification.servicio.models.RespuestaDatosCrear;
import co.com.tuya.certification.servicio.questions.CodigoEstado;
import co.com.tuya.certification.servicio.questions.RespuestaExitosaCrear;
import co.com.tuya.certification.servicio.tasks.ServicioCrear;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.hamcrest.Matchers;

import java.util.List;

import static co.com.tuya.certification.servicio.utils.RutaServicio.BASE_SERVICIO;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class CrearServicioStepDefinitions {

    @Before
    public void iniciar(){
        OnStage.setTheStage(Cast.whereEveryoneCan(CallAnApi.at(BASE_SERVICIO)));
        OnStage.theActorCalled("Usuario");
    }


    @Cuando("^El usuario ingresa la siguiente informacion$")
    public void elUsuarioIngresaLaSiguienteInformacion(List<DatosCrear> crear) {
        OnStage.theActorInTheSpotlight().attemptsTo(ServicioCrear.servicioCrear(crear));
    }

    @Entonces("^El usuario obtendra un codigo de respuesta (.*)$")
    public void elUsuarioObtendraUnCodigoDeRespuesta(int arg) {
        OnStage.theActorInTheSpotlight().should(
                seeThatResponse("The service response is successful", response -> response.statusCode(arg))
        );
    }

    @Entonces("^El usuario podra ver la siguiente informacion$")
    public void elUsuarioPodraVerLaSiguienteInformacion(List<RespuestaDatosCrear> datos) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(RespuestaExitosaCrear.respuestaExitosaCrear(datos.get(0)))
        );
    }

}
