package co.com.tuya.certification.servicio.stepdefinitions;

import co.com.tuya.certification.servicio.questions.CodigoEstado;
import co.com.tuya.certification.servicio.questions.RespuestaExitosaObtener;
import co.com.tuya.certification.servicio.tasks.ServicioObtener;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.hamcrest.Matchers;

import static co.com.tuya.certification.servicio.utils.RutaServicio.BASE_SERVICIO;
import static net.serenitybdd.rest.SerenityRest.useRelaxedHTTPSValidation;

public class ObtenerServicioStepDefinitions {

    @Before
    public void iniciar(){
        OnStage.setTheStage(Cast.whereEveryoneCan(CallAnApi.at(BASE_SERVICIO)));
        OnStage.theActorCalled("Usuario");
    }

    @Cuando("^El usuario hace la peticion al servicio$")
    public void elUsuarioHaceLaPeticionAlServicio() {
        OnStage.theActorInTheSpotlight().attemptsTo(ServicioObtener.servicioGet());
    }

    @Entonces("^El usuario debera ver el codigo de estado (.*)$")
    public void elUsuarioDeberaVerElCodigoDeEstado(int arg1) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(CodigoEstado.codigoEstado(arg1), Matchers.is(true))
        );
    }

    @Entonces("^El usuario obtendra una respuesta exitosa$")
    public void elUsuarioObtendraUnaRespuestaExitosa() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(RespuestaExitosaObtener.respuestaExitosaObtener())
        );
    }
}
