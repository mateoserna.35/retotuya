package co.com.tuya.certification.servicio.utils;

public class RutaServicio {
    public static final String BASE_SERVICIO = "https://reqres.in";
    public static final String OBTENER_SERVICIO = "/api/users?page=1";
    public static final String CREAR_SERVICIO = "/api/users";
    public static final String BASE_CREAR = "https://reqres.in/api/users";
}
