package co.com.tuya.certification.servicio.questions;

import co.com.tuya.certification.servicio.models.RespuestaDatosCrear;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;
import java.util.Map;

public class RespuestaExitosaCrear implements Question<Boolean> {

    RespuestaDatosCrear datos;
    public RespuestaExitosaCrear(RespuestaDatosCrear datos) {
        this.datos = datos;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Map<String, Object> respuestaServicio = SerenityRest.lastResponse().getBody().jsonPath().getMap("");
        return respuestaServicio.get("name").equals(datos.getName())
                && respuestaServicio.get("job").equals(datos.getJob())
                && respuestaServicio.get("id").equals(datos.getId())
                && respuestaServicio.get("createdAt").equals(datos.getCreatedAt());
    }

    public static RespuestaExitosaCrear respuestaExitosaCrear(RespuestaDatosCrear datos){
        return new RespuestaExitosaCrear(datos);
    }
}
