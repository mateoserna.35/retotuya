package co.com.tuya.certification.servicio.models;

public class DatosObtener {
    String id;
    String email;
    String first_name;
    String last_name;
    String avatar;

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAvatar() {
        return avatar;
    }
}
