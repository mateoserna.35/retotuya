package co.com.tuya.certification.servicio.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.Objects;

public class RespuestaExitosaObtener implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return Objects.nonNull(SerenityRest.lastResponse().getBody());
    }

    public static RespuestaExitosaObtener respuestaExitosaObtener(){
        return new RespuestaExitosaObtener();
    }
}
