package co.com.tuya.certification.servicio.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CodigoEstado implements Question <Boolean>{
    private int arg;

    public CodigoEstado(int arg) {
        this.arg = arg;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        //System.out.println(SerenityRest.lastResponse().statusCode());
        return SerenityRest.lastResponse().statusCode()== arg;
    }

    public static CodigoEstado codigoEstado(int arg){
        return new CodigoEstado(arg);
    }
}
