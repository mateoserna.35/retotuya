package co.com.tuya.certification.servicio.tasks;

import co.com.tuya.certification.servicio.models.DatosCrear;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

import java.util.List;

import static co.com.tuya.certification.servicio.utils.RutaServicio.CREAR_SERVICIO;

public class ServicioCrear implements Task {

    private final List<DatosCrear> datos;

    public ServicioCrear(List<DatosCrear> datos) {
        this.datos = datos;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(CREAR_SERVICIO)
                        .with(requestSpecification -> requestSpecification
                                .contentType(ContentType.JSON)
                                .body(datos.get(0))
                                .log().all()
                )
        );
    }

    public static ServicioCrear servicioCrear(List<DatosCrear> datos){
        return Tasks.instrumented(ServicioCrear.class, datos);
    }
}
