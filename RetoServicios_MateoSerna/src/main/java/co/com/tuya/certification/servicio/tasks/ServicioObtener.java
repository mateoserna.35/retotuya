package co.com.tuya.certification.servicio.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static co.com.tuya.certification.servicio.utils.RutaServicio.OBTENER_SERVICIO;

public class ServicioObtener implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource(OBTENER_SERVICIO)
        );
    }

    public static ServicioObtener servicioGet(){
        return Tasks.instrumented(ServicioObtener.class);
    }
}
