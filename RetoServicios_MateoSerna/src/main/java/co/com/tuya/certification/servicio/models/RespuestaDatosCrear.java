package co.com.tuya.certification.servicio.models;

public class RespuestaDatosCrear {
    String name;
    String job;
    String id;
    String createdAt;

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public String getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
