package co.com.tuya.certification.reto.stepdefinitions;

import co.com.tuya.certification.reto.models.Datos;
import co.com.tuya.certification.reto.questions.ValidacionNombre;
import co.com.tuya.certification.reto.questions.ValidacionPrecio;
import co.com.tuya.certification.reto.questions.ValidarDescipcion;
import co.com.tuya.certification.reto.questions.ValidarImagen;
import co.com.tuya.certification.reto.tasks.BotonLaptops;
import co.com.tuya.certification.reto.tasks.VerLaptopDell;
import co.com.tuya.certification.reto.tasks.VerLaptopMac;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class VisualizarProductoStepDefinitions {

    @Managed
    WebDriver driver;

    @Before
    public void SetUp(){
        OnStage.setTheStage(Cast.whereEveryoneCan(BrowseTheWeb.with(driver)));
        OnStage.theActorCalled("usuario");
    }

    @Dado("^El usuario se encuentra en el portal Demoblaze$")
    public void elUsuarioSeEncuentraEnElPortalDemoblaze() {
        OnStage.theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com/index.html"));
    }

    @Cuando("^El usuario selecciona un producto de la categoria$")
    public void elUsuarioSeleccionaUnProductoDeLaCategoria() {
        OnStage.theActorInTheSpotlight().attemptsTo(BotonLaptops.entrarMenu());
        OnStage.theActorInTheSpotlight().attemptsTo(VerLaptopMac.verLaptops());
        OnStage.theActorInTheSpotlight().attemptsTo(BotonLaptops.entrarMenu());
        OnStage.theActorInTheSpotlight().attemptsTo(VerLaptopDell.verLaptopDell());
    }

    @Entonces("^El usuario visualiza el nombre y precio del producto$")
    public void elUsuarioVisualizaElNombreYPrecioDelProducto() {
    }

    @Cuando("^El usuario selecciona un producto y categoria$")
    public void elUsuarioSeleccionaUnProductoYCategoria(List<Datos> info) {
        OnStage.theActorInTheSpotlight().attemptsTo(BotonLaptops.entrarMenu());
        OnStage.theActorInTheSpotlight().attemptsTo(VerLaptopMac.verLaptops());
        OnStage.theActorInTheSpotlight().attemptsTo((Performable) ValidacionNombre.obtenerNombre(info));
    }

    @Entonces("^El usuario verifica los siguientes elementos$")
    public void elUsuarioVerificaLosSiguientesElementos(List<Datos> datos) {
        OnStage.theActorInTheSpotlight().should(
                seeThat(ValidacionNombre.validarDatos(),
                        Matchers.equalTo(ValidacionNombre.obtenerNombre(datos)))
        );
    }

    @Entonces("^El usuario verifica el precio sea (.*)$")
    public void elUsuarioVerificaElPrecioSea(String arg1) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(ValidacionPrecio.validarPrecio(), Matchers.equalTo(arg1))
        );
    }

    @Entonces("^El usuario verifica la descripcion del producto (.*)$")
    public void elUsuarioVerificaLaDescripcionDelProductoGHzDualCoreIntelCoreITurboBoostUpToGHzWithMBSharedLCacheConfigurableToGHzDualCoreIntelCoreITurboBoostUpToGHzWithMBSharedLCache(String arg1) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(ValidarDescipcion.validarDescipcion(), Matchers.equalTo(arg1))
        );
    }
}
