package co.com.tuya.certification.reto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ProductosLaptops {
    public static final Target LAPTOP_MAC = Target.the("producto MakBook").
            located(By.xpath("//*[@id=\"tbodyid\"]/div[3]/div/div/h4/a"));

    public static final Target LAPTOP_DELL = Target.the("Producto Dell").
            located(By.xpath("//*[@id=\"tbodyid\"]/div[4]/div/div/h4/a"));

    public static final Target CUADRO_PRECIO = Target.the("Precio del producto").
            located(By.xpath("//*[@id=\"tbodyid\"]/h3"));

    public static final Target CUADRO_DESCRIPCION = Target.the("Descripcion del producto").
            located(By.xpath("//*[@id=\"more-information\"]/p"));

    public static final Target IMAGEN_PRODUCTO = Target.the("Imagen del producto").
            located(By.xpath("//*[@id=\"imgp\"]/div/img"));
}
