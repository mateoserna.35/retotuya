package co.com.tuya.certification.reto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuPrincipal {
    public static final Target BTN_HOME = Target.the("boton de menu principal").
            located(By.xpath("//*[@id=\"nava\"]"));

    public static final Target BTN_LAPTOPS = Target.the("boton para categoria laptops").
            located(By.xpath("//a[contains(text(),'Laptops')]"));
}