package co.com.tuya.certification.reto.tasks;

import co.com.tuya.certification.reto.userinterfaces.MenuPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;

public class BotonLaptops implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(MenuPrincipal.BTN_HOME, isEnabled())
                        .forNoMoreThan(5).seconds(),
                Click.on(MenuPrincipal.BTN_HOME),
                Click.on(MenuPrincipal.BTN_LAPTOPS)
        );
    }

    public static BotonLaptops entrarMenu(){
        return Tasks.instrumented(BotonLaptops.class);
    }
}
