package co.com.tuya.certification.reto.questions;

import co.com.tuya.certification.reto.userinterfaces.ProductosLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class ValidacionPrecio implements Question {

    @Override
    public Object answeredBy(Actor actor) {
        actor.attemptsTo(WaitUntil.the(ProductosLaptops.CUADRO_PRECIO, WebElementStateMatchers.isEnabled())
                .forNoMoreThan(5).seconds());
        return Text.of(ProductosLaptops.CUADRO_PRECIO).viewedBy(actor).asString();
    }

    public static ValidacionPrecio validarPrecio(){
        return new ValidacionPrecio();
    }
}