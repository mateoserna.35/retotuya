package co.com.tuya.certification.reto.questions;

import co.com.tuya.certification.reto.models.Datos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;

import java.util.List;

public class ValidacionNombre implements Question<Boolean> {

    public static Question <String> obtenerNombre(List<Datos> datos){
        return actor -> Text.of((Target) datos).viewedBy(actor).asString();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return null;
    }

    public static ValidacionNombre validarDatos(){
        return new ValidacionNombre();
    }
}
