package co.com.tuya.certification.reto.questions;

import co.com.tuya.certification.reto.userinterfaces.ProductosLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class ValidarImagen implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        actor.attemptsTo(WaitUntil.the(ProductosLaptops.IMAGEN_PRODUCTO, WebElementStateMatchers.isVisible())
                .forNoMoreThan(5).seconds());
        return Text.of(ProductosLaptops.IMAGEN_PRODUCTO).viewedBy(actor).asString();
    }

    public static ValidarImagen validarImagen(){
        return new ValidarImagen();
    }
}
