package co.com.tuya.certification.reto.tasks;

import co.com.tuya.certification.reto.userinterfaces.ProductosLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;

public class VerLaptopDell implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(ProductosLaptops.LAPTOP_MAC, isEnabled())
                        .forNoMoreThan(5).seconds(),
                Click.on(ProductosLaptops.LAPTOP_MAC));
    }

    public static VerLaptopDell verLaptopDell(){
        return Tasks.instrumented(VerLaptopDell.class);
    }
}
