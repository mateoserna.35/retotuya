package co.com.tuya.certification.reto.models;

public class Datos {
    String Categoria;
    String Producto;
    String Precio;
    String Descripcion;

    public String getCategoria() {
        return Categoria;
    }

    public String getProducto() {
        return Producto;
    }

    public String getPrecio() {
        return Precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }
}
