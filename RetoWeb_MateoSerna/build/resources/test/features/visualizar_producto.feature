#language: es
#Autor: mateoserna.35@gmail.com

  Característica: Yo como cliente de Demoblaze necesito visualizar los precios de los productos

    Antecedentes:
      Dado El usuario se encuentra en el portal Demoblaze

    @VisualizarElementosExitoso
    Escenario: El usuario visualiza el precio de un producto seleccionado
      Cuando El usuario selecciona un producto de la categoria
      Entonces El usuario visualiza el nombre y precio del producto

#    @ValidarInformacionProducto
#    Esquema del escenario: El usuario valida el precio y nombre de un producto seleccionado
#      Cuando El usuario selecciona un producto y categoria
#        | Categoria   | Producto   | Precio   | Descripcion   |
#        | <Categoria> | <Producto> | <Precio> | <Descripcion> |
#      Entonces El usuario verifica los siguientes elementos
#        | Precio   | Producto  | Descripcion   |
#        | <Precio> | <Producto | <Descripcion> |
#      Ejemplos:
#        | Categoria | Producto     | Precio | Descripcion                                                                                                                                                                                                                                                                                                 |
#        | Laptops   | MackBook Air | $700    | 1.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB shared L3 cache Configurable to 2.2GHz dual-core Intel Core i7 (Turbo Boost up to 3.2GHz) with 4MB shared L3 cache.                                                                                                                      |
#        | Laptops   | Sony vaio i7 | $790    | REVIEW Sony is so confident that the VAIO S is a superior ultraportable laptop that the company proudly compares the notebook to Apple's 13-inch MacBook Pro. And in a lot of ways this notebook is better, thanks to a lighter weight, higher-resolution display, more storage space, and a Blu-ray drive. |

  @ValidarInfoMac
  Escenario: El usuario valida la informacion general de la laptop MackBook
    Cuando El usuario selecciona un producto de la categoria
    Entonces El usuario verifica el precio sea $700 *includes tax
    Y El usuario verifica la descripcion del producto 1.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB shared L3 cache Configurable to 2.2GHz dual-core Intel Core i7 (Turbo Boost up to 3.2GHz) with 4MB shared L3 cache.

  @ValidarImagen
  Escenario: El usuario valida la imagen correspondiente al nombre del producto
    Cuando El usuario selecciona un producto de la categoria
    Entonces El usuario verifica la imagen sea